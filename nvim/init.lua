-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")

local opt = vim.opt

opt.backup = false
opt.wrap = true
opt.clipboard = "unnamedplus"

vim.g.vimtex_view_method = "skim"
