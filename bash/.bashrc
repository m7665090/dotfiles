# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# source /home/t4/release/dds/dds_login/bashrc.dds

export PATH=/home/t4/apps/easybuild/bin:$PATH
export PYTHONPATH=/home/t4/apps/easybuild/lib/python3.6/site-packages:$PYTHONPATH
export EB_PYTHON=python3.6
export EASYBUILD_TRACE=1
module use /home/t4/apps/easybuild/modules/all

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
HISTCONTROL=ignoredups:ignorespace

# User specific aliases and functions

alias tn="tmux -u new"
alias ta="tmux -u attach"
alias lg="lazygit"
alias inst="cd ~/DDD/ansible && ./ansinstall && cd"
alias upd="cd ~/DDD/ansible && ./ansupdate && cd"
alias sdieka="source /home/t4/release/dds/dds_login/bashrc.dds"
alias rspi='ssh pi@192.168.178.83'
alias amh="ssh corbachhd@amhead.et.utwente.nl"
alias nsm01="ssh corbachhd@nsm01.et.utwente.nl"
alias nsm02="ssh corbachhd@nsm02.et.utwente.nl"
alias nsm03="ssh corbachhd@nsm03.et.utwente.nl"
alias nsm04="ssh corbachhd@nsm04.et.utwente.nl"
alias vi='/usr/local/bin/nvim'
alias md='./mkdaily.sh'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
alias h='history'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/t4/corbachhd/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/t4/corbachhd/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/t4/corbachhd/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/t4/corbachhd/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/agnoster.omp.json)"
