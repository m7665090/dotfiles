sudo apt update
sudo apt install build-essential
sudo apt install -y zsh

# install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> ~/.bashrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# echo 'eval "$(~/.linuxbrew/bin/brew shellenv)"' >> ~/.bashrc
# eval "$(~/.linuxbrew/bin/brew shellenv)"

# install packages (cmake is needed by tmux)
brew install \
  antibody \
  cmake \
  neovim \
  stow \
  yarn \
  fzf \
  ripgrep \
  bat \
  lazygit \
  npm \
  direnv

brew cleanup -v

mv ../.bashrc ../.bashrc.old

# stow dotfiles
stow git
stow nvim
stow tmux
stow bash
stow zsh

# add zsh as a login shell
command -v zsh | sudo tee -a /etc/shells

# use zsh as default shell
sudo chsh -s $(which zsh) $USER

# bundle zsh plugins
antibody bundle < ~/.zsh_plugins.txt > ~/.zsh_plugins.sh

echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> ~/.zshrc

# echo 'eval "$(~/.linuxbrew/bin/brew shellenv)"' >> ~/.zshrc

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins

# Use kitty terminal on MacOS
# [ `uname -s` = 'Darwin' ] && stow kitty
