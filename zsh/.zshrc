# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="apple"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
 zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git 1password brew macos zsh-autosuggestions)

FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

source $ZSH/oh-my-zsh.sh

# User configuration
# Save command history
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
HISTSIZE=2000
SAVEHIST=1000

setopt HIST_IGNORE_DUPS

alias rm='trash'
alias ls='ls --color=auto'
alias ll='ls -la'
alias vi='nvim'
alias lg='lazygit'
alias h='history'
alias tn="tmux -u new"
alias ta="tmux -u attach"
alias md='./mkdaily.sh'
alias amh="ssh -X corbachhd@amhead.et.utwente.nl"
alias nsm01="ssh corbachhd@nsm01.et.utwente.nl"
alias nsm02="ssh corbachhd@nsm02.et.utwente.nl"
alias nsm03="ssh corbachhd@nsm03.et.utwente.nl"
alias nsm04="ssh corbachhd@nsm04.et.utwente.nl"
alias nsm05="ssh corbachhd@nsm05.et.utwente.nl"
alias nsm06="ssh corbachhd@nsm06.et.utwente.nl"
alias xnsm05="ssh -X corbachhd@nsm05.et.utwente.nl"
alias depmat="ssh corbachhd@nsmdepmat.et.utwente.nl"
alias nsmtest="ssh corbachhd@nsmtest.roaming.utwente.nl"
alias ubuntu2='ssh hermanvc@192.168.178.85'
alias rspi='ssh pi@192.168.178.83'
# DIRCOLORS (MacOS)
export CLICOLOR=1
export HOMEBREW_NO_INSTALL_CLEANUP=1

# FZF
export FZF_DEFAULT_COMMAND="rg --files --hidden --glob '!.git'"
export FZF_DEFAULT_OPTS="--height=40% --layout=reverse --border --margin=1 --padding=1"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
eval "$(fzf --zsh)"
eval "$(zoxide init zsh)"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/h.d.vancorbach/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/h.d.vancorbach/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/h.d.vancorbach/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/h.d.vancorbach/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

